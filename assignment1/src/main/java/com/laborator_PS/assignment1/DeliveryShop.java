package com.laborator_PS.assignment1;

import com.laborator_PS.assignment1.repository.OrderRepository;
import com.laborator_PS.assignment1.repository.ProductRepository;
import com.laborator_PS.assignment1.repository.UserRepository;
import com.laborator_PS.assignment1.service.OrderService;
import com.laborator_PS.assignment1.service.UserService;
import com.laborator_PS.assignment1.util.PopulateDB;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EntityScan
@EnableJpaRepositories
@SpringBootApplication
public class DeliveryShop {

	public static void main(String[] args) {
        SpringApplication.run(DeliveryShop.class, args);
    }

    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    CommandLineRunner init(
            UserRepository userRepository,
            ProductRepository productRepository,
            OrderRepository orderRepository,
            OrderService orderService,
            UserService userService,
            PasswordEncoder passwordEncoder
    ) {
        return args -> {
            PopulateDB populateDB = new PopulateDB();
            populateDB.init(
                    userRepository,
                    productRepository,
                    orderRepository,
                    orderService,
                    userService,
                    passwordEncoder
            );
        };
    }

}
