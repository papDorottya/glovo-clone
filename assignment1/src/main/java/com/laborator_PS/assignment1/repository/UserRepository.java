package com.laborator_PS.assignment1.repository;

import com.laborator_PS.assignment1.model.UserModel;
import com.laborator_PS.assignment1.model.UserTypeEnum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<UserModel, Long> {

    UserModel findByUsername(String username);

    UserModel findFirstByEmail(String email);

    UserModel findFirstByPhone(String phone);

    UserModel findByUsernameAndPassword(String username, String password);

    UserModel deleteByEmail(String email);

    List<UserModel> findByType(UserTypeEnum userTypeEnum);
}
