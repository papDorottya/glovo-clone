package com.laborator_PS.assignment1.repository;

import com.laborator_PS.assignment1.model.ProductCategoryEnum;
import com.laborator_PS.assignment1.model.ProductModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends CrudRepository<ProductModel, Long> {

    ProductModel findByName(String name);

    List<ProductModel> findAllByCategory(ProductCategoryEnum category);

    List<ProductModel> findByIdIn(List<Long> ids);
}
