package com.laborator_PS.assignment1.repository;

import com.laborator_PS.assignment1.model.OrderModel;
import com.laborator_PS.assignment1.model.OrderStatusEnum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends CrudRepository<OrderModel, Long> {

    List<OrderModel> findAllByStatus(OrderStatusEnum status);

    List<OrderModel> findAllByCourierId(Long courierId);

    List<OrderModel> findAllByClientId(Long userId);
}
