package com.laborator_PS.assignment1.constants;

public class NotificationEndpoints {
    public static final String CLIENT_ORDER_ADDITION="/topic/socket/client/";
    public static final String COURIER_ORDER_ADDITION="/topic/socket/courier/";
}
