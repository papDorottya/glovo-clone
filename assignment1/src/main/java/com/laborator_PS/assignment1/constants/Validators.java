package com.laborator_PS.assignment1.constants;

public class Validators {
    /*
     * ^                                 # start of line
     * (?=.*[0-9])                       # positive lookahead, digit [0-9]
     * (?=.*[a-z])                       # positive lookahead, one lowercase character [a-z]
     * (?=.*[A-Z])                       # positive lookahead, one uppercase character [A-Z]
     * (?=.*[!@#&()–[{}]:;',?/*~$^+=<>]) # positive lookahead, one of the special character in this [..]
     * .                                 # matches anything
     * {8,30}                            # length at least 8 characters and maximum of 30 characters
     * $                                 # end of line
     */
    public static final String PASSWORD_PATTERN =
            "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,30}$";

    public static final String EMAIL_PATTERN = "^(.+)@(.+).{5,42}$";

    public static final String PHONE_PATTERN = "^[0-9\\-\\+]{10}$";
}
