package com.laborator_PS.assignment1.controller;

import com.laborator_PS.assignment1.model.UserModel;
import com.laborator_PS.assignment1.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public UserModel save(@RequestBody UserModel user) {
        return userService.save(user);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public UserModel findById(@PathVariable("id") Long id) {
        return userService.findById(id);
    }

    @RequestMapping(value = "/findByUsername/{username}", method = RequestMethod.GET)
    @ResponseBody
    public UserModel findByUsername(@PathVariable("username") String username) {
        return userService.findByUsername(username);
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<UserModel> findAll() {
        return userService.findAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteById(@PathVariable("id") Long id) {
        userService.deleteById(id);
    }

    @RequestMapping(value = "/deleteByEmail/{email}", method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteByEmail(@PathVariable("email") String email) {
        userService.deleteByEmail(email);
    }


    @RequestMapping(value = "/activeUsers", method = RequestMethod.GET)
    @ResponseBody
    public long activeUsers() {
        return userService.getActiveUsers();
    }
}
