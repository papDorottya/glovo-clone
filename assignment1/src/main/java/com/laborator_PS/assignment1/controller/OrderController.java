package com.laborator_PS.assignment1.controller;

import com.laborator_PS.assignment1.model.ClientOrder;
import com.laborator_PS.assignment1.model.CourierOrder;
import com.laborator_PS.assignment1.model.OrderModel;
import com.laborator_PS.assignment1.model.OrderStatusEnum;
import com.laborator_PS.assignment1.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public OrderModel save(@RequestBody OrderModel order) {
        return orderService.save(order);
    }

    @RequestMapping(value = "/saveClientOrder", method = RequestMethod.POST)
    @ResponseBody
    public OrderModel saveClientOrder(@RequestBody ClientOrder order) {
        return orderService.saveClientOrder(order);

    }

    @RequestMapping(value = "/saveCourierOrder", method = RequestMethod.POST)
    @ResponseBody
    public OrderModel saveCourierOrder(@RequestBody CourierOrder order) {
        return orderService.saveCourierOrder(order);
    }

    @RequestMapping(value = "/cancelCourierOrder", method = RequestMethod.POST)
    @ResponseBody
    public OrderModel cancelCourierOrder(@RequestBody Long orderId) {
        return orderService.cancelCourierOrder(orderId);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public OrderModel findById(@PathVariable("id") Long id) {
        return orderService.findById(id);
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<OrderModel> findAll() {
        return orderService.findAll();
    }

    @RequestMapping(value = "/findAllByStatus/{status}", method = RequestMethod.GET)
    @ResponseBody
    public List<OrderModel> findAllByStatus(@PathVariable("status") OrderStatusEnum status) {
        return orderService.findAllByStatus(status);
    }

    @RequestMapping(value = "/findAllByCourier/{courierId}", method = RequestMethod.GET)
    @ResponseBody
    public List<OrderModel> findAllByCourier(@PathVariable("courierId") Long courierId) {
        return orderService.findAllByCourier(courierId);
    }

    @RequestMapping(value = "/findAllByClient/{clientId}", method = RequestMethod.GET)
    @ResponseBody
    public List<OrderModel> findAllByClient(@PathVariable("clientId") Long clientId) {
        return orderService.findAllByClient(clientId);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteById(@PathVariable("id") Long id) {
        orderService.deleteById(id);
    }

    @GetMapping("export/{orderId}")
    public ResponseEntity exportClientOrder(@PathVariable Long orderId, @RequestParam String fileType) {
        return ResponseEntity.ok(orderService.exportClientOrder(orderId, fileType));
    }
}
