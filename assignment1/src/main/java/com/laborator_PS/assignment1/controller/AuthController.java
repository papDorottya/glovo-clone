package com.laborator_PS.assignment1.controller;

import com.laborator_PS.assignment1.constants.Validators;
import com.laborator_PS.assignment1.model.UserModel;
import com.laborator_PS.assignment1.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/auth")
public class AuthController {

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public UserModel login(@RequestParam("username") String username, @RequestParam("password") String password) {
        if (username == null || username == "") {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Empty username");
        }
        if (password == null || password == "") {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Empty password");
        }

        UserModel user = userService.findByUsername(username);
        boolean isPasswordMatch = passwordEncoder.matches(password, user.getPassword());

        if (user != null && isPasswordMatch) {
            this.userService.addActiveUser(user.getId());

            return user;
        }

        throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Wrong credentials");
    }

    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    @ResponseBody
    public void logout(@RequestParam("id") Long userId) {
        this.userService.removeActiveUser(userId);
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    @ResponseBody
    public UserModel register(@RequestBody UserModel user) {

        if (user.getUsername().isBlank()) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Empty username");
        }
        if (user.getPassword().isBlank()) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Empty password");
        }
        if (user.getEmail().isBlank()) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Empty email");
        }
        if (user.getPhone().isBlank()) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Empty phone");
        }

        UserModel foundUser = userService.findByUsername(user.getUsername());
        if (foundUser != null || user.getUsername().length() < 3) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Invalid username.");
        }

        Pattern pattern = Pattern.compile(Validators.PASSWORD_PATTERN);
        Matcher matcher = pattern.matcher(user.getPassword());
        if(!matcher.matches()) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Invalid password.");
        }

        UserModel foundUserEmail = userService.findFirstByEmail(user.getEmail());
        Pattern patternEmail = Pattern.compile(Validators.EMAIL_PATTERN);
        Matcher matcherEmail = patternEmail.matcher(user.getEmail());
        if (foundUserEmail != null || !matcherEmail.matches()) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Invalid email.");
        }

        UserModel foundUserPhone = userService.findFirstByPhone(user.getPhone());
        Pattern patternPhone = Pattern.compile(Validators.PHONE_PATTERN);
        Matcher matcherPhone = patternPhone.matcher(user.getPhone());
        if (foundUserPhone != null || !matcherPhone.matches()) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Invalid phone number.");
        }

        String encodedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(encodedPassword);

        UserModel savedUser = userService.save(user);

        if (savedUser != null) {
            return savedUser;
        }

        throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Oups!");
    }
}
