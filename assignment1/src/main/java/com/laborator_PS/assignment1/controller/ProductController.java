package com.laborator_PS.assignment1.controller;

import com.laborator_PS.assignment1.model.ProductCategoryEnum;
import com.laborator_PS.assignment1.model.ProductModel;
import com.laborator_PS.assignment1.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ProductModel save(@RequestBody ProductModel product) {
        return productService.save(product);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ProductModel findById(@PathVariable("id") Long id) {
        return productService.findById(id);
    }

    @RequestMapping(value = "/findByName/{name}", method = RequestMethod.GET)
    @ResponseBody
    public ProductModel findByName(@PathVariable("name") String name) {
        return productService.findByName(name);
    }

    @RequestMapping(value = "/findByIdIn/{ids}", method = RequestMethod.GET)
    @ResponseBody
    public List<ProductModel> findByIdIn(@PathVariable("ids")List<Long> ids) {
        return productService.findByIdIn(ids);
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<ProductModel> findAll() {
        return productService.findAll();
    }

    @RequestMapping(value = "/findAllByCategory/{category}", method = RequestMethod.GET)
    @ResponseBody
    public List<ProductModel> findAllByCategory(@PathVariable("category") ProductCategoryEnum category) {
        return productService.findAllByCategory(category);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteById(@PathVariable("id") Long id) {
        productService.deleteById(id);
    }
}
