package com.laborator_PS.assignment1.service;

import com.laborator_PS.assignment1.model.ProductCategoryEnum;
import com.laborator_PS.assignment1.model.ProductModel;
import com.laborator_PS.assignment1.model.UserModel;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public interface ProductService {

    ProductModel save(ProductModel product);

    ProductModel findById(Long id);

    ProductModel findByName(String name);

    List<ProductModel> findByIdIn(List<Long> ids);

    List<ProductModel> findAll();

    List<ProductModel> findAllByCategory(ProductCategoryEnum category);

    void deleteById(Long productId);

    void deleteAll();
}
