package com.laborator_PS.assignment1.service;

import com.laborator_PS.assignment1.model.ClientOrder;
import com.laborator_PS.assignment1.model.CourierOrder;
import com.laborator_PS.assignment1.model.OrderModel;
import com.laborator_PS.assignment1.model.OrderStatusEnum;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public interface OrderService {

    OrderModel save(OrderModel order);

    OrderModel saveClientOrder(ClientOrder order);

    OrderModel saveCourierOrder(CourierOrder order);

    OrderModel findById(Long id);

    List<OrderModel> findAll();

    List<OrderModel> findAllByStatus(OrderStatusEnum status);

    List<OrderModel> findAllByCourier(Long courierId);

    List<OrderModel> findAllByClient(Long userId);

    void deleteById(Long orderId);

    void deleteAll();

    OrderModel cancelCourierOrder(Long orderId);

    Object exportClientOrder(Long orderId, String fileType);
}
