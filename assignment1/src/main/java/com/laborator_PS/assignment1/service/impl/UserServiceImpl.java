package com.laborator_PS.assignment1.service.impl;

import com.laborator_PS.assignment1.model.UserModel;
import com.laborator_PS.assignment1.model.UserTypeEnum;
import com.laborator_PS.assignment1.repository.UserRepository;
import com.laborator_PS.assignment1.service.UserService;
import com.laborator_PS.assignment1.util.exporter.FileExporter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("userServiceImpl")
public class UserServiceImpl implements UserService {

    private List<UserModel> activeUsers = new ArrayList<UserModel>();

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserModel save(UserModel user) {
        return userRepository.save(user);
    }

    @Override
    public UserModel findById(Long id) {
        return userRepository.findById(id).get();
    }

    @Override
    public List<UserModel> findAll() {
        return (List<UserModel>) userRepository.findAll();
    }

    @Override
    public UserModel findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public UserModel findFirstByEmail(String email) {
        return userRepository.findFirstByEmail(email);
    }

    @Override
    public UserModel findFirstByPhone(String phone) {
        return userRepository.findFirstByPhone(phone);
    }

    @Override
    public List<UserModel> findByType(UserTypeEnum userTypeEnum) {
        return (List<UserModel>) userRepository.findByType(userTypeEnum);
    }

    @Override
    public UserModel findByUsernameAndPassword(String username, String password) {
        return userRepository.findByUsernameAndPassword(username, password);
    }

    @Override
    public void deleteById(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public void deleteByEmail(String email) {
        userRepository.deleteByEmail(email);
    }

    @Override
    public void deleteAll() {
        userRepository.deleteAll();
    }

    @Override
    public void addActiveUser(Long userId) {
        UserModel user = this.userRepository.findById(userId).get();
        this.activeUsers.add(user);
    }

    @Override
    public void removeActiveUser(Long userId) {
        this.activeUsers.removeIf(u -> u.getId() == userId);
    }

    @Override
    public long getActiveUsers() {
        return this.activeUsers.size();
    }
}
