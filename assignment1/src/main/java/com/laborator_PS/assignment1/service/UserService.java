package com.laborator_PS.assignment1.service;

import com.laborator_PS.assignment1.model.ProductModel;
import com.laborator_PS.assignment1.model.UserModel;
import com.laborator_PS.assignment1.model.UserTypeEnum;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface UserService {

    UserModel save(UserModel user);

    UserModel findById(Long id);

    List<UserModel> findAll();

    UserModel findByUsername(String username);

    UserModel findFirstByEmail(String email);

    UserModel findFirstByPhone(String phone);

    List<UserModel> findByType(UserTypeEnum userTypeEnum);

    UserModel findByUsernameAndPassword(String username, String password);

    void deleteById(Long id);

    void deleteByEmail(String email);

    void deleteAll();

    void addActiveUser(Long userId);

    void removeActiveUser(Long userId);

    long getActiveUsers();
}
