package com.laborator_PS.assignment1.service.impl;

import com.laborator_PS.assignment1.constants.FileType;
import com.laborator_PS.assignment1.constants.NotificationEndpoints;
import com.laborator_PS.assignment1.model.*;
import com.laborator_PS.assignment1.repository.OrderRepository;
import com.laborator_PS.assignment1.repository.ProductRepository;
import com.laborator_PS.assignment1.repository.UserRepository;
import com.laborator_PS.assignment1.service.EmailService;
import com.laborator_PS.assignment1.service.OrderService;
import com.laborator_PS.assignment1.util.exporter.FileExporter;
import com.laborator_PS.assignment1.util.exporter.XMLFileExporter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@Service("orderServiceImpl")
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SimpMessagingTemplate template;

    private EmailService emailService = new EmailService();

    @Override
    public OrderModel save(OrderModel order) {
        if(order.getRating() == 0) {
            this.template.convertAndSend(NotificationEndpoints.CLIENT_ORDER_ADDITION + order.getClient().getId(), "The order status is updated." + order.getStatus() + " "+ order.getId());
        }
        return orderRepository.save(order);
    }

    @Override
    public OrderModel saveClientOrder(ClientOrder model) {
        List<ProductModel> products = new ArrayList<>();
        for (long productId: model.productIds) {
            products.add(productRepository.findById(productId).get());
        }
        UserModel client = userRepository.findById(model.clientId).get();

        OrderModel order = new OrderModel(null, model.status, model.totalPrice, 0, products, null, client);
        OrderModel newOrder = orderRepository.save(order);

        List<OrderModel> orders = new ArrayList<>();
        orders.add(newOrder);

        client.setOrders(orders);
        userRepository.save(client);

        this.template.convertAndSend(NotificationEndpoints.COURIER_ORDER_ADDITION, "A new order has been created.");

        this.emailService.send(client.getEmail(), order.toString());
        return orderRepository.save(newOrder);
    }

    @Override
    public OrderModel saveCourierOrder(CourierOrder model) {
        UserModel courier = userRepository.findById(model.courierId).get();

        OrderModel order = orderRepository.findById(model.orderId).get();

        order.setCourier(courier);
        order.setStatus(model.status);

        this.template.convertAndSend(NotificationEndpoints.CLIENT_ORDER_ADDITION + order.getClient().getId(), "The order status is updated." + model.status + " "+ model.orderId);
        return orderRepository.save(order);
    }

    @Override
    public OrderModel cancelCourierOrder(Long orderId) {
        OrderModel order = orderRepository.findById(orderId).get();
        order.setStatus(OrderStatusEnum.PROCESSING);
        order.setCourier(null);

        this.template.convertAndSend(NotificationEndpoints.CLIENT_ORDER_ADDITION + order.getClient().getId(), "The order status is updated." + order.getStatus() + " "+ order.getId());

        return orderRepository.save(order);
    }

    @Override
    public OrderModel findById(Long id) {
        return orderRepository.findById(id).get();
    }

    @Override
    public List<OrderModel> findAll() {
        return (List<OrderModel>) orderRepository.findAll();
    }

    @Override
    public List<OrderModel> findAllByStatus(OrderStatusEnum status) {
        return orderRepository.findAllByStatus(status);
    }

    @Override
    public List<OrderModel> findAllByCourier(Long courierId) {
        return orderRepository.findAllByCourierId(courierId);
    }

    @Override
    public List<OrderModel> findAllByClient(Long userId) {
        return orderRepository.findAllByClientId(userId);
    }

    @Override
    public void deleteById(Long orderId) {
        orderRepository.deleteById(orderId);
    }

    @Override
    public void deleteAll() {
        orderRepository.deleteAll();
    }

    @Override
    public Object exportClientOrder(Long orderId, String fileType) {
        OrderModel order = this.findById(orderId);
        FileExporter fileExporter;

        if(order == null) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Invalid order id.");
        }

        if(fileType.equals(FileType.XML)) {
            fileExporter = new XMLFileExporter();
            return fileExporter.exportData(order);
        }

        return null;
    }
}
