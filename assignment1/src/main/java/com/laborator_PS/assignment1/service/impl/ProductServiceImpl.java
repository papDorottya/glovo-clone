package com.laborator_PS.assignment1.service.impl;

import com.laborator_PS.assignment1.model.ProductCategoryEnum;
import com.laborator_PS.assignment1.model.ProductModel;
import com.laborator_PS.assignment1.repository.ProductRepository;
import com.laborator_PS.assignment1.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("productServiceImpl")
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public ProductModel save(ProductModel product) {
        return productRepository.save(product);
    }

    @Override
    public ProductModel findById(Long id) {
        return productRepository.findById(id).get();
    }

    @Override
    public ProductModel findByName(String name) {
        return productRepository.findByName(name);
    }

    @Override
    public List<ProductModel> findByIdIn(List<Long> ids) {
        return productRepository.findByIdIn(ids);
    }

    @Override
    public List<ProductModel> findAll() {
        return (List<ProductModel>) productRepository.findAll();
    }

    @Override
    public List<ProductModel> findAllByCategory(ProductCategoryEnum category) {
        return productRepository.findAllByCategory(category);
    }

    @Override
    public void deleteById(Long productId) {
        productRepository.deleteById(productId);
    }

    @Override
    public void deleteAll() {
        productRepository.deleteAll();
    }
}
