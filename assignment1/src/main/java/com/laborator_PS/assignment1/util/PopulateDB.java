package com.laborator_PS.assignment1.util;

import com.laborator_PS.assignment1.model.*;
import com.laborator_PS.assignment1.repository.*;
import com.laborator_PS.assignment1.service.UserService;
import com.laborator_PS.assignment1.service.OrderService;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PopulateDB {
    public void init(
            UserRepository userRepository,
            ProductRepository productRepository,
            OrderRepository orderRepository,
            OrderService orderService,
            UserService userService,
            PasswordEncoder passwordEncoder
    ) {
        // generate admin
        UserModel adminModel = new UserModel(null, "ad", passwordEncoder.encode("ad"), "ad", "123", UserTypeEnum.ADMIN, null);
        // save
        UserModel adminSaved = userRepository.save(adminModel);

        // generate courier
        UserModel courierModel =new UserModel(null, "co", passwordEncoder.encode("co"), "co", "123", UserTypeEnum.COURIER, null);
        // save
        UserModel courierSaved = userRepository.save(courierModel);

        // generate user
        UserModel clientModel = new UserModel(null, "cl", passwordEncoder.encode("cl"), "papdorottya99@gmail.com", "0783912346",  UserTypeEnum.CLIENT, null);
        // save
        UserModel clientSaved = userRepository.save(clientModel);

        // generate products
        ProductModel productModel = ProductModel.builder()
                .name("produs")
                .price(400l)
                .category(ProductCategoryEnum.Clothes)
                .build();
        ProductModel productModel2 = ProductModel.builder()
                .name("produs doi")
                .price(500l)
                .category(ProductCategoryEnum.Clothes)
                .build();
        List<ProductModel> products = new ArrayList<>();
        products.add(productModel);
        products.add(productModel2);
        // save
        List<ProductModel> savedProducts = (List<ProductModel>) productRepository.saveAll(products);

        // generate order
        final Long[] totalOrderPrice = {0l};
        products.forEach(p -> totalOrderPrice[0] += p.getPrice());
        OrderModel order = new OrderModel(null, OrderStatusEnum.FINISHED, totalOrderPrice[0], 5, savedProducts, courierSaved, clientSaved);
        // save
        OrderModel savedOrder = orderRepository.save(order);

        orderRepository.save(new OrderModel(null, OrderStatusEnum.FINISHED, totalOrderPrice[0],0, savedProducts, courierSaved, clientSaved));

        courierSaved.setOrders(Collections.singletonList(savedOrder));
        userRepository.save(courierSaved);
    }
}
