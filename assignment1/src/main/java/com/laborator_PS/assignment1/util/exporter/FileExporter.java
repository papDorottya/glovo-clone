package com.laborator_PS.assignment1.util.exporter;

public interface FileExporter {
    String exportData(Object object);
}
