package com.laborator_PS.assignment1.model;

public enum UserTypeEnum {
    ADMIN, COURIER, CLIENT;
}
