package com.laborator_PS.assignment1.model;

public enum ProductCategoryEnum {
    FoodAndDrinks, Health, Shoes, Clothes, Supermarket, Others;
}
