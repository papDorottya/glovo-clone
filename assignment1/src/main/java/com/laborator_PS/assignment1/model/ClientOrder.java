package com.laborator_PS.assignment1.model;

import java.util.List;

public class ClientOrder {

    public OrderStatusEnum status;

    public Long totalPrice;

    public List<Long> productIds;

    public Long clientId;
}
