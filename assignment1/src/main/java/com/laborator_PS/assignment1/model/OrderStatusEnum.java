package com.laborator_PS.assignment1.model;

public enum OrderStatusEnum {
    PROCESSING, DELIVERING, FINISHED;
}
