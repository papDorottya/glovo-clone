package com.laborator_PS.assignment1.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@XmlRootElement(name="order")
@XmlAccessorType(XmlAccessType.FIELD)
@ToString
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "orders")
public class OrderModel implements Serializable {

    @Id
    @XmlAttribute(name="id")
    @Column(name = "ORDER_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "STATUS")
    @XmlTransient
    @Enumerated(EnumType.STRING)
    private OrderStatusEnum status;

    @Column(name = "TOTALPRICE")
    private Long totalPrice;

    @Column(name = "RATING")
    private int rating;

    @ManyToMany
    @XmlElement(name="product")
    private List<ProductModel> products;

    @ManyToOne(fetch = FetchType.EAGER)
    @XmlTransient
    @JoinColumn(name = "courier_id", referencedColumnName="id")
    @JsonIgnoreProperties("orders")
    private UserModel courier;

    @ManyToOne(fetch = FetchType.EAGER)
    @XmlTransient
    @JoinColumn(name = "client_id", referencedColumnName="id")
    @JsonIgnoreProperties("orders")
    private UserModel client;

    @Override
    public String toString() {
        return "Buna " + client.getUsername() + ",\n\n"
                + "Te anuntam ca am primit comanda ta cu id-ul " + id + " si acum este in procesare." + "\n" +
                "\n" + "Pretul total este de " + totalPrice + " lei.\nIti multumim!";
    }
}
