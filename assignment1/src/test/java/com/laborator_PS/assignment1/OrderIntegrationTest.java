package com.laborator_PS.assignment1;

import com.laborator_PS.assignment1.model.*;
import com.laborator_PS.assignment1.service.OrderService;
import com.laborator_PS.assignment1.service.ProductService;
import com.laborator_PS.assignment1.service.UserService;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "spring.profiles.active=test")
public class OrderIntegrationTest {

    @Autowired
    private OrderService orderService;

    @Autowired
    private UserService userService;

    @Autowired
    private ProductService productService;

    @Before
    public void before() {
        orderService.deleteAll();
    }

    @BeforeEach
    public void beforeEach() {
        UserModel courier = new UserModel(null, "courier111", "courier111", "courier111@gmail.com", "0754712345", UserTypeEnum.COURIER, null);
        userService.save(courier);

        UserModel client = new UserModel(null, "client123", "client123", "client123@gmail.com", "0712212345", UserTypeEnum.CLIENT, null);
        userService.save(client);

        ProductModel product1 = new ProductModel(null, "Burger", 35l, ProductCategoryEnum.FoodAndDrinks);
        productService.save(product1);

        ProductModel product2 = new ProductModel(null, "Happy Birthday Cake", 120l, ProductCategoryEnum.FoodAndDrinks);
        productService.save(product2);
    }

    @AfterEach
    public void afterEach() {
        userService.deleteAll();
        productService.deleteAll();
        orderService.deleteAll();
    }

    @Test
    public void testSave() {
        UserModel courier = userService.findByType(UserTypeEnum.COURIER).get(0);
        UserModel client = userService.findByType(UserTypeEnum.CLIENT).get(0);

        assertEquals(0, orderService.findAll().size());

        OrderModel orderModel = new OrderModel(null, OrderStatusEnum.PROCESSING, 155l, 5, productService.findAllByCategory(ProductCategoryEnum.FoodAndDrinks), courier, client);
        orderService.save(orderModel);

        assertEquals(1, orderService.findAll().size());
    }

    @Test
    public void testDelete() {
        UserModel courier = userService.findByType(UserTypeEnum.COURIER).get(0);
        UserModel client = userService.findByType(UserTypeEnum.CLIENT).get(0);

        OrderModel orderModel = new OrderModel(null, OrderStatusEnum.PROCESSING,155l, 4, productService.findAllByCategory(ProductCategoryEnum.FoodAndDrinks), courier, client);
        OrderModel savedOrderModel = orderService.save(orderModel);

        assertEquals(1, orderService.findAll().size());

        orderService.deleteById(savedOrderModel.getId());

        assertEquals(0, orderService.findAll().size());
    }

    @Test
    public void testFind() {
        UserModel courier = userService.findByType(UserTypeEnum.COURIER).get(0);
        UserModel client = userService.findByType(UserTypeEnum.CLIENT).get(0);

        assertEquals(0, orderService.findAll().size());

        OrderModel orderModel = new OrderModel(null, OrderStatusEnum.PROCESSING,155l, 3, productService.findAllByCategory(ProductCategoryEnum.FoodAndDrinks), courier, client);
        OrderModel savedOrderModel = orderService.save(orderModel);
        Long orderId = savedOrderModel.getId();

        assertEquals(orderId, orderService.findById(orderId).getId());
    }
}
