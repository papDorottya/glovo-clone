package com.laborator_PS.assignment1;

import com.laborator_PS.assignment1.model.ProductCategoryEnum;
import com.laborator_PS.assignment1.model.ProductModel;
import com.laborator_PS.assignment1.model.UserModel;
import com.laborator_PS.assignment1.model.UserTypeEnum;
import com.laborator_PS.assignment1.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "spring.profiles.active=test")
public class UserIntegrationTest {

    @Autowired
    private UserService userService;

    @Before
    public void before() {
        userService.deleteAll();
    }

    @AfterEach
    public void afterEach() {
        userService.deleteAll();
    }

    @Test
    public void testSave() {
        assertEquals(0, userService.findAll().size());

        UserModel userModel = new UserModel(null, "clientA", "clientA", "clientA@gmail.com", "0724567891", UserTypeEnum.CLIENT, null);
        userService.save(userModel);

        assertEquals(1, userService.findAll().size());
    }

    @Test
    public void testDelete() {
        UserModel userModel = new UserModel(null, "clientA", "clientA", "clientA@gmail.com", "0724567891", UserTypeEnum.CLIENT, null);
        UserModel savedUserModel = userService.save(userModel);

        assertEquals(1, userService.findAll().size());

        userService.deleteById(savedUserModel.getId());

        assertEquals(0, userService.findAll().size());
    }

    @Test
    public void testFind() {
        assertEquals(0, userService.findAll().size());

        UserModel userModel = new UserModel(null, "clientA", "clientA", "clientA@gmail.com", "0724567891", UserTypeEnum.CLIENT, null);
        UserModel savedUserModel = userService.save(userModel);
        Long usertId = savedUserModel.getId();

        assertEquals(usertId, userService.findById(usertId).getId());
    }
}
