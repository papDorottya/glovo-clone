package com.laborator_PS.assignment1;

import com.laborator_PS.assignment1.model.*;
import com.laborator_PS.assignment1.service.OrderService;
import com.laborator_PS.assignment1.service.ProductService;
import com.laborator_PS.assignment1.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "spring.profiles.active=test")
public class ProductIntegrationTest {

    @Autowired
    private ProductService productService;

    @Before
    public void before() {
        productService.deleteAll();
    }

    @AfterEach
    public void afterEach() {
        productService.deleteAll();
    }

    @Test
    public void testSave() {
        assertEquals(0, productService.findAll().size());

        ProductModel productModel = new ProductModel(null, "Bluza Nike XS", 90l, ProductCategoryEnum.Clothes);
        productService.save(productModel);

        assertEquals(1, productService.findAll().size());
    }

    @Test
    public void testDelete() {
        ProductModel productModel = new ProductModel(null, "Bluza Nike XS", 90l, ProductCategoryEnum.Clothes);
        ProductModel savedProductModel = productService.save(productModel);

        assertEquals(1, productService.findAll().size());

        productService.deleteById(savedProductModel.getId());

        assertEquals(0, productService.findAll().size());
    }

    @Test
    public void testFind() {
        assertEquals(0, productService.findAll().size());

        ProductModel productModel = new ProductModel(null, "Bluza Nike XS", 90l, ProductCategoryEnum.Clothes);
        ProductModel savedProductModel = productService.save(productModel);
        Long productId = savedProductModel.getId();

        assertEquals(productId, productService.findById(productId).getId());
    }
}
