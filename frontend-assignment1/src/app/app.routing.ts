﻿import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { RegisterComponent } from './common/auth/register/register.component';
import { LoginComponent } from './common/auth/login/login.component';
import { AuthGuard } from './core/guards/auth.guard';
import { APP_ROUTES } from './core/routes/routes';
import {AdminComponent} from './admin/admin.component';
import {CreateProductComponent} from './admin/products/create-product/create-product.component';
import {NotFoundComponent} from './common/not-found/not-found.component';
import {ClientComponent} from './client/client.component';
import {CourierComponent} from './courier/courier.component';

const routes: Routes = [
    { path: '', component: AppComponent, canActivate: [AuthGuard] },
    { path: APP_ROUTES.LOGIN.path, component: LoginComponent },
    { path: APP_ROUTES.REGISTER.path, component: RegisterComponent },
    { path: APP_ROUTES.ADMIN.path, component: AdminComponent },
    { path: APP_ROUTES.ADMIN.CREATE_PRODUCT.path, component: CreateProductComponent },
    { path: APP_ROUTES.CLIENT.path, component: ClientComponent },
    { path: APP_ROUTES.COURIER.path, component: CourierComponent },
    { path: APP_ROUTES.NOT_FOUND.path, component: NotFoundComponent },

    // otherwise redirect to home
    { path: '**', redirectTo: APP_ROUTES.NOT_FOUND.path }
];

export const appRoutingModule = RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' });
