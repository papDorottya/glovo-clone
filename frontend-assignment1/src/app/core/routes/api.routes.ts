export const API_ROUTES = {
    ALL: '/',
    LOGIN: '/auth/login',
    LOGOUT: '/auth/logout',
    REGISTER: '/auth/register',
    USER: '/user',
    ORDER: '/order',
    PRODUCT: '/product',
};

export function ApiUrl(path) {
    return `http://localhost:8080${path}`;
}

export function UserUrl(path?: string) {
    return `${ApiUrl(API_ROUTES.USER)}${path ? path : ''}`;
}

export function OrderUrl(path?: string) {
    return `${ApiUrl(API_ROUTES.ORDER)}${path ? path : ''}`;
}

export function ProductUrl(path?: string) {
    return `${ApiUrl(API_ROUTES.PRODUCT)}${path ? path : ''}`;
}
