export const APP_ROUTES = {
  LOGIN: {
    url: '/login',
    path: 'login'
  },
  REGISTER: {
    url: '/register',
    path: 'register'
  },
  CLIENT: {
    url: '/client',
    path: 'client'
  },
  ADMIN: {
    url: '/admin',
    path: 'admin',
    CREATE_PRODUCT : {
      url: "/admin/create-product",
      path: "admin/create-product"
    }
  },
  COURIER: {
    url: '/courier',
    path: 'courier'
  },
  NOT_FOUND: {
    url: '/not-found',
    path: 'not-found'
  }
};
