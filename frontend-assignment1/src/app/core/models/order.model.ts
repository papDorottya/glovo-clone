import {OrderStatusEnum} from './orderStatus.enum';
import {User} from './user.model';
import {Product} from './product.model';

export class Order {
  public id: number;
  public status: OrderStatusEnum;
  public totalPrice: number;
  public rating: number;
  public products: Product[];
  public courier: User;
  public client: User;
  public expanded: boolean;
}
