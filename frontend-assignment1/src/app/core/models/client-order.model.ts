import {OrderStatusEnum} from './orderStatus.enum';

export class ClientOrder {
  public status: OrderStatusEnum;
  public totalPrice: number;
  public productIds: number[];
  public clientId: number;
}
