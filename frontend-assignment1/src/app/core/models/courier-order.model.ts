import {OrderStatusEnum} from './orderStatus.enum';

export class CourierOrder {
  public status: OrderStatusEnum;
  public courierId: number;
  public orderId: number;
}
