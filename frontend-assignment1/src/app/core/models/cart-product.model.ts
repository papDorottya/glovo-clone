import {Product} from './product.model';

export class CartProduct extends Product {
  public quantity: number;
}
