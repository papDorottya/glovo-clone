import {UserTypeEnum} from './userType.enum';
import {Order} from './order.model';

export class User {
  public id: number;
  public username: string;
  public password: string;
  public email: string;
  public phone: string;
  public type: UserTypeEnum;
  public orders: Order[];
}
