import {ProductCategoryEnum} from './productCategory.enum';

export class Product {
  public id: number;
  public name: string;
  public price: number;
  public category: ProductCategoryEnum;
}
