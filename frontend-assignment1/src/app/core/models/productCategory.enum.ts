export enum ProductCategoryEnum {
  FoodAndDrinks = 'FoodAndDrinks', Health = 'Health', Shoes = 'Shoes', Clothes = 'Clothes', Supermarket = 'Supermarket', Others = 'Others'
}
