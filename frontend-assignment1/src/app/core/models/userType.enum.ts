export enum UserTypeEnum {
  ADMIN = 'ADMIN',
  COURIER = 'COURIER',
  CLIENT = 'CLIENT'
}
