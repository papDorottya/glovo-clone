﻿import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { APP_ROUTES } from '../routes/routes';
import {AuthService} from '../services/auth.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private authService: AuthService
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = this.authService.getUser();
        if (currentUser) {
            // authorised so return true
            return true;
        }

        // not logged in so redirect to login page
        this.router.navigate([APP_ROUTES.LOGIN.url]);
        return false;
    }
}
