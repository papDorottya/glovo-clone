import {Injectable} from '@angular/core';
import {Stomp} from '@stomp/stompjs';
import SockJS from 'sockjs-client';

@Injectable({ providedIn: 'root' })
export class ClientSocketService {
  private callbackFunctions: Array<{(notification: any): void; }> = [];

  constructor(
  ) {
  }

  public subscribeToNotifications(clientId: number) {
    const URL="http://localhost:8080/socket";

    const websocket = new SockJS(URL);

    const stompClient = Stomp.over(websocket);

    stompClient.connect({}, () => (
      stompClient.subscribe('/topic/socket/client/'+ clientId, notification => {
        this.callbackFunctions.forEach(s => s(notification))
      })
    ))
  }

  public subscribeToWebsocket(callbackFunction: (notification) => void) {
    this.callbackFunctions.push(callbackFunction);
  }
}
