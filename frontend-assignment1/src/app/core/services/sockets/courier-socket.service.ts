import {Injectable} from '@angular/core';
import {Stomp} from '@stomp/stompjs';
import SockJS from 'sockjs-client';

@Injectable({ providedIn: 'root' })
export class CourierSocketService {
  private callbackFunctions: Array<{(notification: any): void; }> = [];

  constructor() {
    this.subscribeToNotifications();
  }

  private subscribeToNotifications() {
    const URL="http://localhost:8080/socket";

    const websocket = new SockJS(URL);

    const stompCourier = Stomp.over(websocket);

    stompCourier.connect({}, () => (
      stompCourier.subscribe('/topic/socket/courier/', notification => {
        this.callbackFunctions.forEach(s => s(notification))
      })
    ))
  }

  public subscribeToWebsocket(callbackFunction: (notification) => void) {
    this.callbackFunctions.push(callbackFunction);
  }
}
