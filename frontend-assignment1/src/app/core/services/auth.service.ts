﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ApiUrl, API_ROUTES} from '../routes/api.routes';
import {User} from '../models/user.model';

@Injectable({providedIn: 'root'})
export class AuthService {

  constructor(private http: HttpClient) {
  }

  async login(username, pass) {
    const response: any = await this.http.post(ApiUrl(API_ROUTES.LOGIN) + `?username=${username}&password=${pass}`, {}).toPromise();

    const user: User = response;
    if (user !== undefined) {
      // store user details and jwt token in local storage to keep user logged in between page refreshes
      localStorage.setItem('user', JSON.stringify(user));
      localStorage.setItem('type', user.type.toString());
      return user;
    }

    throw response.message;
  }

  async register(user: User) {
    return await this.http.post(ApiUrl(API_ROUTES.REGISTER), user).toPromise();
  }

  async logout() {
    const user = this.getUser();
    // remove user from local storage and set current user to null
    localStorage.removeItem('user');
    localStorage.removeItem('type');

    await this.http.post(ApiUrl(API_ROUTES.LOGOUT) + `?id=${user.id}`, {}).toPromise();
  }

  public getUser() {
    return <User>JSON.parse(localStorage.getItem('user'));
  }


  isUserLoggedIn() {
    return !!this.getUser();
  }
}
