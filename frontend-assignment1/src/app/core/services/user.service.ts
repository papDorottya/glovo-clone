﻿import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {User} from '../models/user.model';
import {UserUrl} from '../routes/api.routes';
import {UserTypeEnum} from '../models/userType.enum';

@Injectable({providedIn: 'root'})
export class UserService {
  constructor(
    private http: HttpClient
  ) {
  }

  save(user: User) {
    return this.http.post<User>(UserUrl(), user).toPromise();
  }

  findById(id: number) {
    return this.http.get<User>(UserUrl('/' + id)).toPromise();
  }

  findAll() {
    return this.http.get<User[]>(UserUrl()).toPromise();
  }

  findByUsername(username: string) {
    return this.http.get<User>(UserUrl('/' + username)).toPromise();
  }

  findByType(userTypeEnum: UserTypeEnum) {
    return this.http.get<User>(UserUrl('/findByUsername/' + userTypeEnum)).toPromise();
  }

  deleteById(id: number) {
    return this.http.delete<void>(UserUrl('/' + id)).toPromise();
  }

  deleteByEmail(email: string) {
    return this.http.delete<void>(UserUrl('/deleteByEmail/' + email)).toPromise();
  }

  getUserTypes() {
    return Object.values(UserTypeEnum);
  }

  getActiveUsers() {
    return this.http.get<number>(UserUrl('/activeUsers')).toPromise();
  }
}
