﻿import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Product} from '../models/product.model';
import {ProductUrl} from '../routes/api.routes';
import {ProductCategoryEnum} from '../models/productCategory.enum';

@Injectable({providedIn: 'root'})
export class ProductService {
  constructor(
    private http: HttpClient
  ) {
  }

  save(product: Product) {
    return this.http.post<Product>(ProductUrl(), product).toPromise();
  }

  findById(id: number) {
    return this.http.get<Product>(ProductUrl('/' + id)).toPromise();
  }

  findAll() {
    return this.http.get<Product[]>(ProductUrl()).toPromise();
  }

  findByName(name: string) {
    return this.http.get<Product>(ProductUrl('/' + name)).toPromise();
  }

  findByIdIn(ids: string[]) {
    let idsString: string = '';// 1,2,3

    ids.forEach((id, index) => idsString += id + (index < ids.length-1 ? ',' : ''))

    return this.http.get<Product[]>(ProductUrl('/findByIdIn/' + idsString)).toPromise();
  }

  findAllByCategory(category: ProductCategoryEnum) {
    return this.http.get<Product[]>(ProductUrl('/findAllByCategory/' + category)).toPromise();
  }

  deleteById(id: number) {
    return this.http.delete<void>(ProductUrl('/' + id)).toPromise();
  }

  getAllCategoryies() {
    return Object.values(ProductCategoryEnum);
  }
}
