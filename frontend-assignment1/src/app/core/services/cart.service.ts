import {Injectable} from '@angular/core';
import {Product} from '../models/product.model';
import {CartProduct} from '../models/cart-product.model';

@Injectable({ providedIn: 'root' })
export class CartService {
  public cartProductsIds: any = {};

  constructor() { }

  addToCart(id: string) {
    if (this.cartProductsIds[id])
      this.cartProductsIds[id]++;
    else
      this.cartProductsIds[id] = 1;
  }

  removeFromCart(id: string) {
    if (this.cartProductsIds[id] === 1)
      delete this.cartProductsIds[id];
    else
      this.cartProductsIds[id]--;
  }

  resetCart() {
    this.cartProductsIds = {};
  }

  getCartProductIds() {
    return Object.keys(this.cartProductsIds);
  }

  getAllCartProductIds() {
    const ids = [];

    this.getCartProductIds()
      .forEach(id => {
        for (let index=0; index < this.cartProductsIds[id]; index++) {
          ids.push(id);
        }
      });

    return ids;
  }

  getCartProducts(products: Product[]) {
    let cartProducts: CartProduct[] = [];

    products.forEach(product => {
      let cartProduct = <CartProduct>product;
      cartProduct.quantity = this.cartProductsIds[product.id];
      cartProducts.push(cartProduct);
    });

    return cartProducts;
  }

}
