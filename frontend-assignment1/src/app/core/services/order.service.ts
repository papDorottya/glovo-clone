﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {OrderUrl} from '../routes/api.routes';
import {Order} from '../models/Order.model';
import {OrderStatusEnum} from '../models/orderStatus.enum';
import {ClientOrder} from '../models/client-order.model';
import {CourierOrder} from '../models/courier-order.model';

@Injectable({providedIn: 'root'})
export class OrderService {
  constructor(
    private http: HttpClient,
  ) {
  }

  save(order: Order) {
    return this.http.post<Order>(OrderUrl(), order).toPromise();
  }

  saveClientOrder(order: ClientOrder) {
    return this.http.post<Order>(OrderUrl('/saveClientOrder'), order).toPromise();
  }

  saveCourierOrder(order: CourierOrder) {
    return this.http.post<Order>(OrderUrl('/saveCourierOrder'), order).toPromise();
  }

  findById(id: number) {
    return this.http.get<Order>(OrderUrl('/' + id)).toPromise();
  }

  findAll() {
    return this.http.get<Order[]>(OrderUrl()).toPromise();
  }

  findAllByCourier(id: number) {
    return this.http.get<Order[]>(OrderUrl('/findAllByCourier/' + id)).toPromise();
  }

  findAllByClient(id: number) {
    return this.http.get<Order[]>(OrderUrl('/findAllByClient/' + id)).toPromise();
  }

  findAllByStatus(status: OrderStatusEnum) {
    return this.http.get<Order>(OrderUrl('/findAllByStatus/' + status)).toPromise();
  }

  deleteById(id: number) {
    return this.http.delete<void>(OrderUrl('/' + id)).toPromise();
  }

  getAllStatuses() {
    return Object.values(OrderStatusEnum);
  }

  cancelOrder(orderId: number) {
    return this.http.post<Order>(OrderUrl('/cancelCourierOrder'), orderId).toPromise();
  }

  retrieveData(orderId: number, fileType: string) {
    return this.http.get(OrderUrl('/export/' + orderId), {
      params: {fileType: fileType},
      responseType: 'text'
    }).toPromise();
  }
}
