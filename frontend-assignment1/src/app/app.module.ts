import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {appRoutingModule} from './app.routing';
import {HttpClientModule} from '@angular/common/http';
import {AdminComponent} from './admin/admin.component';
import {ProductsComponent} from './admin/products/products.component';
import {AppHeaderComponent} from './common/app-header/app-header.component';
import {RegisterComponent} from './common/auth/register/register.component';
import {LoginComponent} from './common/auth/login/login.component';
import {CreateProductComponent} from './admin/products/create-product/create-product.component';
import {OrdersComponent} from './admin/orders/orders.component';
import {UsersComponent} from './admin/users/users.component';
import {NotFoundComponent} from './common/not-found/not-found.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {ClientComponent} from './client/client.component';
import {CartComponent} from './client/cart/cart.component';
import {AccountComponent} from './client/account/account.component';
import {ClientOrdersComponent} from './client/orders/orders.component';
import {ClientProductsComponent} from './client/products/products.component';
import {CourierComponent} from './courier/courier.component';
import {AccountCourierComponent} from './courier/account-courier/account-courier.component';
import {OrdersCourierComponent} from './courier/orders-courier/orders-courier.component';
import {AvailableOrdersComponent} from './courier/available-orders/available-orders.component';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { StarRatingComponent } from './client/orders/star-rating/star-rating.component';
import {MatIconModule} from '@angular/material/icon';
import {MatTooltipModule} from '@angular/material/tooltip';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    AppHeaderComponent,

    AdminComponent,
    ProductsComponent,
    CreateProductComponent,
    OrdersComponent,
    UsersComponent,
    NotFoundComponent,
    ClientComponent,
    CartComponent,
    AccountComponent,
    ClientOrdersComponent,
    ClientProductsComponent,
    CourierComponent,
    AccountCourierComponent,
    OrdersCourierComponent,
    AvailableOrdersComponent,
    StarRatingComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    appRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatSelectModule,
    MatCardModule,
    MatButtonModule,
    MatSnackBarModule,
    MatIconModule,
    MatTooltipModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
