import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';


@Component({
  selector: 'app-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.css']
})
export class StarRatingComponent implements OnInit {

  @Input()
  public rating: number;

  @Input()
  public isEditing: boolean;

  @Output()
  public ratingUpdated = new EventEmitter<string>();

  public ratingArr: string[] = [];

  constructor() {
  }


  ngOnInit() {
    for (let index = 1; index < 6; index++) {
      this.ratingArr.push(index.toString());
    }
  }

  showIcon(index: string) {
    if (this.rating >= parseInt(index)) {
      return 'star';
    } else {
      return 'star_border';
    }
  }

  update(rating: string) {
    this.ratingUpdated.emit(rating);
  }
}

