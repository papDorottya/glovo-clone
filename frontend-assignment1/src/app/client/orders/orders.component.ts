import {Component, OnInit} from '@angular/core';
import {Order} from '../../core/models/order.model';
import {OrderService} from '../../core/services/order.service';
import {AuthService} from '../../core/services/auth.service';
import {ClientSocketService} from '../../core/services/sockets/client-socket.service';
import {saveAs} from 'file-saver';
import {OrderStatusEnum} from '../../core/models/orderStatus.enum';

@Component({
  selector: 'app-client-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class ClientOrdersComponent implements OnInit {
  public orders: Order[] = [];
  public id: number;
  public orderStatusFinished: OrderStatusEnum = OrderStatusEnum.FINISHED;

  constructor(
    public orderService: OrderService,
    public authService: AuthService,
    public clientSocketService: ClientSocketService

  ) { }

  async ngOnInit() {
   await this.loadData();
    this.subscribeToNotifications();
  }

  private subscribeToNotifications() {
    this.clientSocketService.subscribeToWebsocket(async (_) => {
      await this.loadData();
    });
  }

  private async loadData() {
    const user = this.authService.getUser();
    this.orders = await this.orderService.findAllByClient(user.id);
  }

  public async exportData(id: number) {
    const data = await this.orderService.retrieveData(id, 'xml');

    if(data) {
      let typeForBlob = 'text/xml;charset=utf-8';
      let blob = new Blob([data], {type: typeForBlob});
      saveAs(blob, "owner-data." + 'xml');
    }
  }

  public async updateOrderRating(rating: string, order: Order) {
    order.rating = parseInt(rating);
    await this.orderService.save(order);
  }
}
