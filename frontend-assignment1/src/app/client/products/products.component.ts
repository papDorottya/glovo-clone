import { Component, OnInit } from '@angular/core';
import {Product} from '../../core/models/product.model';
import {CartService} from '../../core/services/cart.service';
import {ProductService} from '../../core/services/product.service';
import {ProductCategoryEnum} from '../../core/models/productCategory.enum';

@Component({
  selector: 'app-client-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ClientProductsComponent implements OnInit {
  public products: Product[] = [];
  public categories: string[] = [];
  public activeCategory: string;
  public allProductsString = 'All products';

  constructor(
    public cartService: CartService,
    public productService: ProductService
  ) { }

  async ngOnInit() {
    this.categories = await this.productService.getAllCategoryies();
    await this.getAllProducts();
  }

  addToCart(product: Product) {
    this.cartService.addToCart(product.id.toString());
  }

  async getProductsByCategory(category: string) {
    this.activeCategory = category;
    this.products = await this.productService.findAllByCategory(<ProductCategoryEnum>category);
  }

  async getAllProducts() {
    this.activeCategory = this.allProductsString;
    this.products = await this.productService.findAll();
  }
}
