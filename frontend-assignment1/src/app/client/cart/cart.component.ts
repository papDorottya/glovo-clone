import {Component, OnInit} from '@angular/core';
import {CartProduct} from '../../core/models/cart-product.model';
import {CartService} from '../../core/services/cart.service';
import {OrderService} from '../../core/services/order.service';
import {ProductService} from '../../core/services/product.service';
import {Order} from '../../core/models/order.model';
import {User} from '../../core/models/user.model';
import {OrderStatusEnum} from '../../core/models/orderStatus.enum';
import {Product} from '../../core/models/product.model';
import {AuthService} from '../../core/services/auth.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  public products: CartProduct[] = [];
  public totalPrice: number = 0;

  constructor(
    public cartService: CartService,
    public orderService: OrderService,
    public productService: ProductService,
    private authService: AuthService
  ) { }

  async ngOnInit() {
    const cardProductsIds = this.cartService.getCartProductIds();
    if (cardProductsIds.length) {
      const products = await this.productService.findByIdIn(cardProductsIds);
      this.products = this.cartService.getCartProducts(products);
    } else {
      this.products = [];
    }

    this.calculateTotalPrice();
  }

  async finishOrder() {
    let userId = this.authService.getUser().id;

    await this.orderService.saveClientOrder({
      clientId: userId,
      status: OrderStatusEnum.PROCESSING,
      totalPrice: this.totalPrice,
      productIds: this.cartService.getAllCartProductIds()
    });
    this.cartService.resetCart();

    await this.ngOnInit();
  }

  calculateTotalPrice() {
    this.totalPrice = 0;
    this.products.forEach(product => this.totalPrice += product.price * product.quantity);
  }

  async removeItem(id: string) {
    this.cartService.removeFromCart(id);

    await this.ngOnInit();
  }
}
