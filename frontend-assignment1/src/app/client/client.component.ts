import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ClientTab} from './clientTab.enum';
import {CartService} from '../core/services/cart.service';
import SockJS from 'sockjs-client';
import {Stomp} from '@stomp/stompjs';
import {MatSnackBar} from '@angular/material/snack-bar';
import {AuthService} from '../core/services/auth.service';
import {ClientSocketService} from '../core/services/sockets/client-socket.service';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {
  public activeTab: ClientTab = ClientTab.Products;
  public clientTab = ClientTab;

  constructor(
    private route: ActivatedRoute,
    public cartService: CartService,
    private snackBar: MatSnackBar,
    public authService: AuthService,
    public clientSocketService: ClientSocketService
  ) { }

  ngOnInit() {
    const tab: ClientTab = this.route.snapshot.paramMap.get('tab') as ClientTab;

    if (tab !== null) {
      this.activeTab = tab;
    }
    this.subscribeToWebsocket();
    this.subscribeToNotifications();
  }

  public get getCartTotalProductsCount() {
    return this.cartService.getCartProductIds().length;
  }

  private subscribeToWebsocket() {
    const user = this.authService.getUser();
    this.clientSocketService.subscribeToNotifications(user.id);
  }

  private subscribeToNotifications() {
    this.clientSocketService.subscribeToWebsocket((notification) => {
      let message = notification.body;
      this.snackBar.open(message, 'Close', {
        duration: 3000
      })
    });
  }
}
