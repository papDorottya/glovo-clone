import { Component, OnInit } from '@angular/core';
import {UserService} from '../../core/services/user.service';
import {User} from '../../core/models/user.model';
import {AuthService} from '../../core/services/auth.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {APP_ROUTES} from '../../core/routes/routes';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  public user: User;

  public formGroup: FormGroup = new FormGroup({});

  public updateErrorMessage: string;

  constructor(
    public userService: UserService,
    public authService: AuthService
  ) { }

  async ngOnInit() {
    const user = this.authService.getUser();
    this.user = await this.userService.findById(user.id);

    this.setupFormGroup();
  }

  async updateAccount() {
    if(!this.formGroup.valid) {
      return
    }
    try {
      const user: User = new User();

      user.id = this.user.id;
      user.username = this.usernameForm.value;
      user.password = this.passwordForm.value;
      user.email = this.emailForm.value;
      user.phone = this.phoneForm.value;

      await this.userService.save(user);
    } catch (err: any) {
      this.updateErrorMessage = err.error.message;
    }
  }
  private setupFormGroup() {
    this.formGroup = new FormGroup({
      username: new FormControl({value: this.user.username, disabled: true}),
      // ^ - start of string (implicit in string regex pattern)
      // (?=\D*\d) - there must be 1 digit
      // (?=[^a-z]*[a-z]) - there must be 1 lowercase ASCII letter
      // (?=[^A-Z]*[A-Z]) - there must be 1 uppercase ASCII letter
      // .{8,30} - any 8 to 30 chars other than line break chars
      // $ - end of string (implicit in string regex pattern).
      password: new FormControl(this.user.password, [Validators.required, Validators.pattern(/^(?=\D*\d)(?=[^a-z]*[a-z])(?=[^A-Z]*[A-Z]).{8,30}$/)]),
      email: new FormControl(this.user.email, [Validators.required, Validators.email, Validators.minLength(5)]),
      phone: new FormControl(this.user.phone, [Validators.required, Validators.pattern(/^[0-9]{10,10}$/)]),
    });
  }

  public get emailForm() {
    return this.formGroup.controls.email;
  }

  public get phoneForm() {
    return this.formGroup.controls.phone;
  }

  public get passwordForm() {
    return this.formGroup.controls.password;
  }

  public get usernameForm() {
    return this.formGroup.controls.username;
  }
}
