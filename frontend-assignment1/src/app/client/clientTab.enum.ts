export enum ClientTab {
  Orders = "Orders",
  Products = "Products",
  Cart = "Cart",
  Account = "Account"
}
