import { Component, OnInit } from '@angular/core';
import {Order} from '../../core/models/order.model';
import {OrderService} from '../../core/services/order.service';
import {AuthService} from '../../core/services/auth.service';
import {OrderStatusEnum} from '../../core/models/orderStatus.enum';

@Component({
  selector: 'app-orders-courier',
  templateUrl: './orders-courier.component.html',
  styleUrls: ['./orders-courier.component.css']
})
export class OrdersCourierComponent implements OnInit {
  public orders: Order[] = [];
  public orderStatuses: OrderStatusEnum[] = [];
  public orderStatus = OrderStatusEnum;

  constructor(
    public orderService: OrderService,
    public authService: AuthService
  ) { }

  async ngOnInit() {
    this.orderStatuses = <OrderStatusEnum[]>this.orderService.getAllStatuses();

    await this.loadData();
  }

  async updateOrderStatus(order: Order) {
    await this.orderService.save(order);
  }

  async cancelOrder(id: number) {
    await this.orderService.cancelOrder(id);
    await this.loadData();
  }

  private async loadData() {
    const user = this.authService.getUser();
    this.orders = await this.orderService.findAllByCourier(user.id);
  }
}
