import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {CourierTab} from './courierTab.enum';
import {MatSnackBar} from '@angular/material/snack-bar';
import {CourierSocketService} from '../core/services/sockets/courier-socket.service';

@Component({
  selector: 'app-courier',
  templateUrl: './courier.component.html',
  styleUrls: ['./courier.component.css']
})
export class CourierComponent implements OnInit {
  public activeTab: CourierTab = CourierTab.AvailableOrders;
  public courierTab = CourierTab;

  constructor(
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private courierSocketService: CourierSocketService
  ) { }

  ngOnInit() {
    const tab: CourierTab = this.route.snapshot.paramMap.get('tab') as CourierTab;

    if (tab !== null) {
      this.activeTab = tab;
    }

    this.subscribeToNotifications();
  }

  private subscribeToNotifications() {
    this.courierSocketService.subscribeToWebsocket((notification) => {
      let message = notification.body;

      this.snackBar.open(message, 'Close', {
        duration: 3000
      })
    })
  }


}
