import {Component, OnInit} from '@angular/core';
import {Order} from '../../core/models/order.model';
import {OrderStatusEnum} from '../../core/models/orderStatus.enum';
import {OrderService} from '../../core/services/order.service';
import {AuthService} from '../../core/services/auth.service';
import {CourierSocketService} from '../../core/services/sockets/courier-socket.service';

@Component({
  selector: 'app-available-orders',
  templateUrl: './available-orders.component.html',
  styleUrls: ['./available-orders.component.css']
})
export class AvailableOrdersComponent implements OnInit {
  public orders: Order[] = [];
  public orderStatuses: OrderStatusEnum[] = [];

  constructor(
    private orderService: OrderService,
    private authService: AuthService,
    private courierSocketService: CourierSocketService
  ) { }

  async ngOnInit() {
    await this.loadData();
    this.subscribeToWebSocket();

  }

  async saveCourierOrder(orderId: number) {
    let userId = this.authService.getUser().id;

    await this.orderService.saveCourierOrder({
      courierId: userId,
      orderId: orderId,
      status: OrderStatusEnum.DELIVERING
    });

    await this.loadData();
  }

  private async loadData() {
    const orders = await this.orderService.findAll();
    this.orders = orders.filter(o => !o.courier);
    this.orderStatuses = <OrderStatusEnum[]>this.orderService.getAllStatuses();
  }

  private subscribeToWebSocket() {
    this.courierSocketService.subscribeToWebsocket(async (notification) => {
      await this.loadData();
    })
  }
}
