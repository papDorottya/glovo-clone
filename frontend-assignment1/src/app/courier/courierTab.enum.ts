export enum CourierTab {
  Orders = "Orders",
  Account = "Account",
  AvailableOrders = "AvailableOrders"
}
