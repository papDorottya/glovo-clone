import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {APP_ROUTES} from 'src/app/core/routes/routes';
import {AuthService} from 'src/app/core/services/auth.service';
import {UserService} from '../../core/services/user.service';
import {UserTypeEnum} from '../../core/models/userType.enum';

@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.css']
})
export class AppHeaderComponent implements OnInit {
  public activeUsers: number = 0;

  constructor(
    private router: Router,
    public authenticationService: AuthService,
    public userService: UserService
  ) {
  }

  async ngOnInit(){
    this.activeUsers = await this.userService.getActiveUsers();
  }

  async logout() {
    await this.authenticationService.logout();
    this.router.navigate([APP_ROUTES.LOGIN.url]);
  }

  public get isUserAdmin() {
    return this.authenticationService.getUser()?.type === UserTypeEnum.ADMIN;
  }
}
