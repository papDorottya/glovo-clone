import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserTypeEnum } from 'src/app/core/models/userType.enum';
import { APP_ROUTES } from 'src/app/core/routes/routes';
import { AuthService } from 'src/app/core/services/auth.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  public formGroup: FormGroup = new FormGroup({});

  public loginErrorMessage: string;

  constructor(
    private router: Router,
    private authenticationService: AuthService
  ) {}

  public ngOnInit() {
    this.setupFormGroup();
  }

  public async register() {
    this.router.navigate([APP_ROUTES.REGISTER.url]);
  }

  public async login() {
    if(!this.formGroup.valid) {
      return
    }

    try {
      let user = await this.authenticationService.login(this.usernameForm.value, this.passwordForm.value);
      if(user !== undefined) {
        if(user.type === UserTypeEnum.ADMIN) {
          this.router.navigate([APP_ROUTES.ADMIN.url]);
        } else if(user.type === UserTypeEnum.CLIENT){
          this.router.navigate([APP_ROUTES.CLIENT.url]);
        } else if(user.type === UserTypeEnum.COURIER){
          this.router.navigate([APP_ROUTES.COURIER.url]);
        }
      }
    } catch (err: any) {
      if(err.status == 401) {
        this.loginErrorMessage = err.error.message;
      }
    }
  }

  private setupFormGroup() {
    this.formGroup = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
    });
  }

  public get passwordForm() {
    return this.formGroup.controls.password;
  }

  public get usernameForm() {
    return this.formGroup.controls.username;
  }
}
