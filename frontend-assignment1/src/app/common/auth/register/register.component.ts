import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { APP_ROUTES } from 'src/app/core/routes/routes';
import {AuthService} from '../../../core/services/auth.service';
import {UserTypeEnum} from '../../../core/models/userType.enum';
import {UserService} from '../../../core/services/user.service';
import {User} from '../../../core/models/user.model';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public formGroup: FormGroup = new FormGroup({});

  public types: UserTypeEnum[] = [];

  public registerErrorMessage: string;

  constructor(
    private router: Router,
    private authService: AuthService,
    private userService: UserService
  ){}

  public ngOnInit() {
    this.setupTypes();

    this.setupFormGroup();
  }

  public async register() {
    if(!this.formGroup.valid) {
      return
    }

    try {
      const user: User = new User();

      user.username = this.usernameForm.value;
      user.password = this.passwordForm.value;
      user.email = this.emailForm.value;
      user.phone = this.phoneForm.value;
      user.type = this.typeForm.value;

      await this.authService.register(user);
      this.router.navigate([APP_ROUTES.LOGIN.url]);
    } catch (err: any) {
      this.registerErrorMessage = err.error.message;
    }
  }

  private setupTypes() {
    this.types = this.userService.getUserTypes();
  }

  private setupFormGroup() {
    this.formGroup = new FormGroup({
      username: new FormControl('', [Validators.required, Validators.minLength(3)]),
      // ^ - start of string (implicit in string regex pattern)
      // (?=\D*\d) - there must be 1 digit
      // (?=[^a-z]*[a-z]) - there must be 1 lowercase ASCII letter
      // (?=[^A-Z]*[A-Z]) - there must be 1 uppercase ASCII letter
      // .{8,30} - any 8 to 30 chars other than line break chars
      // $ - end of string (implicit in string regex pattern).
      password: new FormControl('', [Validators.required, Validators.pattern(/^(?=\D*\d)(?=[^a-z]*[a-z])(?=[^A-Z]*[A-Z]).{8,30}$/)]),
      email: new FormControl('', [Validators.required, Validators.email, Validators.minLength(5)]),
      phone: new FormControl('', [Validators.required, Validators.pattern(/^[0-9]{10,10}$/)]),
      type: new FormControl(this.types[1], [Validators.required])
    });
  }

  public get emailForm() {
    return this.formGroup.controls.email;
  }

  public get phoneForm() {
    return this.formGroup.controls.phone;
  }

  public get passwordForm() {
    return this.formGroup.controls.password;
  }

  public get usernameForm() {
    return this.formGroup.controls.username;
  }

  public get typeForm() {
    return this.formGroup.controls.type;
  }
}
