import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from 'src/app/core/models/product.model';
import { ProductCategoryEnum } from 'src/app/core/models/productCategory.enum';
import { APP_ROUTES } from 'src/app/core/routes/routes';
import { AdminTab } from '../../AdminTab.enum';
import {ProductService} from '../../../core/services/product.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.css']
})
export class CreateProductComponent implements OnInit {
  public product: Product = new Product();
  public categories: ProductCategoryEnum[] = [];

  public formGroup: FormGroup = new FormGroup({});

  public createProductErrorMessage: string;

  constructor(
    private productService: ProductService,
    private router: Router
  ) { }

  ngOnInit() {
    this.setupCategories();

    this.setupFormGroup();
  }

  async addProduct() {
    if(!this.formGroup.valid) {
      return;
    }

    try {
      const product: Product = new Product();
      console.log(product);

      product.name = this.nameForm.value;
      console.log(product);

      product.price = this.priceForm.value;
      console.log(product);

      product.category = this.categoryForm.value;

      console.log(product);
      await this.productService.save(product);
      await this.router.navigate([APP_ROUTES.ADMIN.url, { tab: AdminTab.Products }]);
    } catch (err: any) {
      this.createProductErrorMessage = err.error.message;
    }
  }
  private setupFormGroup() {
    this.formGroup = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.minLength(2)]),
      price: new FormControl('', [Validators.required, Validators.min(1)]),
      category: new FormControl(this.categories[1]),
    });
  }

  private setupCategories() {
    this.categories = <ProductCategoryEnum[]> this.productService.getAllCategoryies();
  }

  public get priceForm() {
    return this.formGroup.controls.price;
  }

  public get nameForm() {
    return this.formGroup.controls.name;
  }

  public get categoryForm() {
    return this.formGroup.controls.category;
  }

}
