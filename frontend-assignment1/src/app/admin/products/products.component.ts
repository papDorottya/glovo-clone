import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from 'src/app/core/models/product.model';
import { ProductCategoryEnum } from 'src/app/core/models/productCategory.enum';
import { APP_ROUTES } from 'src/app/core/routes/routes';
import {ProductService} from '../../core/services/product.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  public formGroup: FormGroup = new FormGroup({});

  public products: Product[] = [];
  public categories: ProductCategoryEnum[] = [];

  constructor(
    private productService: ProductService,
    private router: Router
  ) { }

  async ngOnInit() {
    await this.loadData();

    this.setupForm();
  }

  async updateProduct(product: Product, formGroup: FormGroup) {
    if (!formGroup.valid) {
      return;
    }
    const updatedProduct: Product = new Product();

    updatedProduct.id = product.id;
    updatedProduct.name = formGroup.controls.name.value;
    updatedProduct.price = formGroup.controls.price.value;
    updatedProduct.category = product.category;


    await this.productService.save(updatedProduct);
  }

  async deleteProduct(product: Product) {
    await this.productService.deleteById(product.id);
    await this.ngOnInit();
  }

  async addNewProducts() {
    await this.router.navigate([APP_ROUTES.ADMIN.CREATE_PRODUCT.url]);
  }

  private setupForm() {
    this.products.forEach(p => {
      this.formGroup.addControl(p.id.toString(), new FormGroup({
        name: new FormControl(p.name, [Validators.required, Validators.minLength(2)]),
        price: new FormControl(p.price, [Validators.required, Validators.min(1)]),
      }))
    });
  }

  private async loadData() {
    this.products = await this.productService.findAll();
    this.categories = <ProductCategoryEnum[]>this.productService.getAllCategoryies();
  }
  public getFormGroup(formGroupId: number) {
    return (this.formGroup.controls[formGroupId.toString()] as FormGroup);
  }

  public nameForm(formGroupId: number) {
    return this.getFormGroup(formGroupId).controls.name as FormControl;
  }

  public priceForm(formGroupId: number) {
    return this.getFormGroup(formGroupId).controls.price as FormControl;
  }
}
