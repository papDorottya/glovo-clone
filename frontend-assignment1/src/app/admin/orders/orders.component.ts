import { Component, OnInit } from '@angular/core';
import {Order} from '../../core/models/order.model';
import {OrderStatusEnum} from '../../core/models/orderStatus.enum';
import {OrderService} from '../../core/services/order.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  public orders: Order[] = [];
  public orderStatuses: OrderStatusEnum[] = [];

  constructor(
    private orderService: OrderService
  ) { }

  async ngOnInit() {
    this.orders = await this.orderService.findAll();
    this.orderStatuses = <OrderStatusEnum[]>this.orderService.getAllStatuses();
  }

  async updateOrderStatus(order: Order) {
    await this.orderService.save(order);
  }

}
