import { Component, OnInit } from '@angular/core';
import {UserService} from '../../core/services/user.service';
import {User} from '../../core/models/user.model';
import {UserTypeEnum} from '../../core/models/userType.enum';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  public users: User[] = [];
  public types: UserTypeEnum[] = [];

  public formGroup: FormGroup = new FormGroup({});

  constructor(
    private userService: UserService
  ) { }

  async ngOnInit() {
    await this.loadData();

    this.setupForm();
  }

  async updateUser(user: User, formGroup: FormGroup) {
    if (!formGroup.valid) {
      formGroup.markAllAsTouched();
      return;
    }

    const updatedUser: User = new User();

    updatedUser.id = user.id;
    updatedUser.username = formGroup.controls.username.value;
    updatedUser.password = formGroup.controls.password.value;
    updatedUser.email = formGroup.controls.email.value;
    updatedUser.phone = formGroup.controls.phone.value;
    updatedUser.type = formGroup.controls.type.value;

    await this.userService.save(updatedUser);
  }

  async deleteUser(user: User) {
    await this.userService.deleteById(user.id);
    await this.ngOnInit();
  }

  private setupForm() {
    this.users.forEach(u => {
      this.formGroup.addControl(u.id.toString(), new FormGroup({
        username: new FormControl(u.username, [Validators.required, Validators.minLength(3)]),
        // ^ - start of string (implicit in string regex pattern)
        // (?=\D*\d) - there must be 1 digit
        // (?=[^a-z]*[a-z]) - there must be 1 lowercase ASCII letter
        // (?=[^A-Z]*[A-Z]) - there must be 1 uppercase ASCII letter
        // .{8,30} - any 8 to 30 chars other than line break chars
        // $ - end of string (implicit in string regex pattern).
        password: new FormControl(u.password, [Validators.required, Validators.pattern(/^(?=\D*\d)(?=[^a-z]*[a-z])(?=[^A-Z]*[A-Z]).{8,30}$/)]),
        email: new FormControl(u.email, [Validators.required, Validators.email, Validators.minLength(5)]),
        phone: new FormControl(u.phone, [Validators.required, Validators.pattern(/^[0-9]{10,10}$/)]),
        type: new FormControl(u.type, [Validators.required])
      }))
    });
  }

  private async loadData() {
    this.users = await this.userService.findAll();
    this.types = <UserTypeEnum[]>this.userService.getUserTypes();
  }
  public getFormGroup(formGroupId: number) {
    return (this.formGroup.controls[formGroupId.toString()] as FormGroup);
  }

  public usernameForm(formGroupId: number) {
    return this.getFormGroup(formGroupId).controls.username as FormControl;
  }

  public passwordForm(formGroupId: number) {
    return this.getFormGroup(formGroupId).controls.password as FormControl;
  }

  public emailForm(formGroupId: number) {
    return this.getFormGroup(formGroupId).controls.email as FormControl;
  }

  public phoneForm(formGroupId: number) {
    return this.getFormGroup(formGroupId).controls.phone as FormControl;
  }

  public typeForm(formGroupId: number) {
    return this.getFormGroup(formGroupId).controls.type as FormControl;
  }
}
